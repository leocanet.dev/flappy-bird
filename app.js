let canvas = document.getElementById("canvas");
let context = canvas.getContext("2d");

let bird = new Image();
let backg = new Image();
let floor = new Image();
let pipe_top = new Image();
let pipe_bot = new Image();

bird.src = "images/bird.png";
backg.src = "images/bg.png";
floor.src = "images/fg.png";
pipe_top.src = "images/pipeNorth.png";
pipe_bot.src = "images/pipeSouth.png";

let gap = 125;
let birdX = 10;
let birdY = 150;
let constant;
let gravity = 2.5;
let score = 0;


let fly_sound = new Audio();
let score_sound = new Audio();

fly_sound.src = "sounds/fly.mp3";
score_sound.src = "sounds/score.mp3";

let pipe = [];

pipe[0] = {
    x : canvas.width,
    y : 0
};

document.addEventListener("keydown",jump);

function jump(){
    birdY -= 50;
    fly_sound.play();
}

function start() {
    context.drawImage(backg,0,0);

    for(let i = 0; i < pipe.length; i++){
        constant = pipe_top.height + gap;
        context.drawImage(pipe_top, pipe[i].x, pipe[i].y);
        context.drawImage(pipe_bot, pipe[i].x, pipe[i].y+constant);

        pipe[i].x--;

        if(pipe[i].x == 400){
            pipe.push({
                x : canvas.width,
                y : Math.floor(Math.random()*pipe_top.height-(pipe_bot.height/2))
            });
        }

        if (birdX + bird.width >= pipe[i].x && birdX <= pipe[i].x + pipe_top.width && (birdY <= pipe[i].y + pipe_top.height || birdY + bird.height >= pipe[i].y + constant) || birdY + bird.height >= canvas.height - floor.height) {
            location.reload();
        }

        if(pipe[i].x == 5){
            score++;
            score_sound.play();
        }

    }

    context.drawImage(bird, birdX, birdY);
    
    let pat=context.createPattern(floor,"repeat");      // repeat the image as a pattern
    context.fillStyle=pat;                              // set the fill style
    context.rect(0, 590, canvas.height, floor.height);  // create a rectangle
    context.fill();                                     // fill it with the pattern.

    birdY += gravity;

    context.fillStyle = "#000";
    context.font = "30px Verdana";

    context.fillText("Score : " + score, 10, canvas.height-20)

    requestAnimationFrame(start);

}

start();